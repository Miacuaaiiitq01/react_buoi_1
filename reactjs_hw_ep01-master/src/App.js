import "./App.css";
import Banner from "./BTComponent/Banner";
import Body from "./BTComponent/Body";
import Footer from "./BTComponent/Footer";
import Header from "./BTComponent/Header";
import Home from "./BTComponent/Home";
import Item from "./BTComponent/Item";
import style from "./asset/style.module.css";
function App() {
  return (
    <div className="App">
      <Home></Home>
      <Item/>
      <Footer/>
      <Header/>
      <Body/>
      <Banner/>
    </div>
  );
}

export default App;
