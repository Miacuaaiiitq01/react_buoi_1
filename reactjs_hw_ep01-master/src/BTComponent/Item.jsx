import React from "react";
import style from "../asset/style.module.css";

const Item = () => {
  return (
    <div className={style.item}>
      <div className={style.container}>
        <div className={style.card}>
          <div className={style.cardContainer}>
            <div>
              <img
                src="https://is1-ssl.mzstatic.com/image/thumb/Purple126/v4/63/36/1b/63361bcc-d787-6174-d3cc-dcfb436ce8f2/AppIcon-1x_U007emarketing-0-7-0-P3-85-220.png/246x0w.webp"
                alt=""
                className={style.icon}
              />
            </div>
            <div className={style.cardTitle}>Fresh new layout</div>
            <p className={style.cardText}>
              With Bootstrap 5, we've created a fresh new layout for this
              template!
            </p>
          </div>
        </div>
        <div className={style.card}>
          <div className={style.cardContainer}>
            <div>
              <img
                src="https://play-lh.googleusercontent.com/YXWTgfNZF7lMRzfzVzkDRgLPU_BBlCDeWZiLPUWUj-jogNT4ftmHJwlV8cUlxBmn8Q=w240-h480-rw"
                alt=""
                className={style.icon}
              />
            </div>
            <div className={style.cardTitle}>Free to download</div>
            <p className={style.cardText}>
              As always, Start Bootstrap has a powerful collectin of free
              templates.
            </p>
          </div>
        </div>
        <div className={style.card}>
          <div className={style.cardContainer}>
            <div>
              <img
                src="https://play-lh.googleusercontent.com/2LW7kQvpt6_2y1MwtZFBU4zs8IS0a9nLqQ2JP2r7R-rhNKyt8Db8lrdxS3qSaJA-Yp0=w240-h480-rw"
                alt=""
                className={style.icon}
              />
            </div>
            <div className={style.cardTitle}>Jumbotron hero header</div>
            <p className={style.cardText}>
              The heroic part of this template is the jumbotron hero header!
            </p>
          </div>
        </div>
        <div className={style.card}>
          <div className={style.cardContainer}>
            <div>
              <img
                src="https://www.emojiall.com/en/header-svg/%F0%9F%87%A7.svg"
                alt=""
                className={style.icon}
              />
            </div>
            <div className={style.cardTitle}>Feature boxes</div>
            <p className={style.cardText}>
              We've created some custom feature boxes using Bootstrap icons!
            </p>
          </div>
        </div>
        <div className={style.card}>
          <div className={style.cardContainer}>
            <div>
              <img
                src="https://lh3.googleusercontent.com/IkadzttFb1dCoVBuV-MgbNqltXsDKA_wGKvkkqZcidl6J0CQcdN0EXF1bj4KRT6wkyLQeGmkokLTLgaL2RX9DmKj=w128-h128-e365-rj-sc0x00ffffff"
                alt=""
                className={style.icon}
              />
            </div>
            <div className={style.cardTitle}>Simple clean code</div>
            <p className={style.cardText}>
              We keep our dependencies up to date and squash bugs as they come!
            </p>
          </div>
        </div>
        <div className={style.card}>
          <div className={style.cardContainer}>
            <div>
              <img
                src="https://play-lh.googleusercontent.com/HstUa1VemLGLop_1ad8aAviAqEBPynQVqaZAff_0ZiKiAfmPw0DlB7_PWz8X90Kd38w=w240-h480-rw"
                alt=""
                className={style.icon}
              />
            </div>
            <div className={style.cardTitle}>A name you trust</div>
            <p className={style.cardText}>
              Start Bootstrap has been the leader in free Bootstrap templates
              since 2013!
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Item;
