import React from "react";
import style from "../asset/style.module.css";

const Header = () => {
  return (
    <div className={style.header}>
      <div className={style.container}>
        <div className={style.nav}>
          <div className={style.nav_right}>
            <h2 style={{ fontWeight: "400" }}>Start Bootstrap</h2>
          </div>
          <div className="nav_left">
            <a href="" className={style.link}>
              Home
            </a>
            <a href="" className={style.link}>
              About
            </a>
            <a href="" className={style.link}>
              Contact
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
