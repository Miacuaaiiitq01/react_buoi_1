import React from "react";
import styleBanner from "../asset/style.module.css";

const Banner = () => {
  return (
    <div className={styleBanner.banner}>
      <div className={styleBanner.container}>
        <h2 className={styleBanner.bannerTitle}>A Warm Welcome!</h2>
        <p className={styleBanner.bannerText}>
          Bootstrap utility classes are used to create this jumbotron since the
          old component has been removed from the framework. Why create custom
          CSS when you can use utilities?
        </p>
        <button className={styleBanner.bannerBtn}>Call to action</button>
      </div>
    </div>
  );
};

export default Banner;
