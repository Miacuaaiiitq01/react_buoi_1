import React from "react";
import Header from "./Header";
import Body from "./Body.jsx";
import Footer from "./Footer";

const Home = () => {
  return (
    <div>
      <Header></Header>
      <Body></Body>
      <Footer></Footer>
    </div>
  );
};

export default Home;
